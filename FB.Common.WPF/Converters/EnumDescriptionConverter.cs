﻿using System;
using System.ComponentModel;
using System.Globalization;
using System.Reflection;
using System.Windows.Data;
using System.Linq;

namespace FB.Common.WPF.Converters
{
    public class EnumDescriptionConverter : IValueConverter
    {
        object IValueConverter.Convert(object value, Type target_type, object parameter, CultureInfo culture) => this.get_enum_description((Enum)value);

        private string get_enum_description(Enum enum_obj)
        {
            FieldInfo field_info = enum_obj.GetType().GetField(enum_obj.ToString());
            object[] custom_attributes = field_info.GetCustomAttributes(false);

            var custom_attribute = custom_attributes.OfType<DescriptionAttribute>().FirstOrDefault();
            return custom_attribute is DescriptionAttribute description_attribute ? description_attribute.Description : enum_obj.ToString();
        }

        object IValueConverter.ConvertBack(object value, Type target_type, object parameter, CultureInfo culture) => string.Empty;
    }
}
